<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Request\StoreUserPost;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
  /** 
   * login api 
   * 
   * @return \Illuminate\Http\Response 
   */ 
  public function login(){ 
    if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) { 
      $user = Auth::user(); 
      $token = $user->createToken('MyApp')->accessToken;
      $response = [
        'message' => 'Login successfully.', 
        'token' => $token,
        'status' => true
      ];

      return response()->json($response); 
    } else { 
      return response()->json(['error'=>'Sorry, wrong email or password'], 400); 
    } 
  }

  /** 
   * Register api 
   * 
   * @return \Illuminate\Http\Response 
   */ 
  public function register(StoreUserPost $request) 
  { 
    $input = $request->all(); 
    $input['password'] = Hash::make($input['password']); 
    $user = User::create($input);
    
    return response()->json(['message'=> 'Registered successfully.', 'status' => true]); 
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::all();
    return response()->json(['data' => $users, 'status' => true]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreUserPost $request): Response
  {
    User::create($request);
    return response()->json(['message'=>'Created successfully.', 'status' => true]);
  }

  /**
   * Display the specified resource.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
    return response(['data' => $user, 'status' => true]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, User $user)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    //
  }
}
